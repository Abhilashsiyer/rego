# Rego REST service

This a REST service to create and retrieve regitrations 


## Steps to see the desired output

1. Start the Springboot using gradle bootrun
2. Create a registration by sending a POST request to /api/v1/regos with body 

```
{          
        "plate_number":"dsdsd",
            "registration": {
                "expired": false,
                 "expiry_date": "2093-02-05T23:15:30.000Z"
            },
            "vehicle": {
                "type": 2232,
                "make": "dd",
                "model": "X4 M40i",
                "colour": "Blue",
                "vin": "12389347324",
                "tare_weight": 1700,
                "gross_mass": 123
            },
            "insurer": {
                "name": "cdc",
                "code": 23
                }
        }
```

This will create one registration entry to the database. You may create as many entries you like by keying in the desired values in the request above.

3.To retrieve all registrations, Send a GET request to /api/v1/regos . If there are two entries for example, following will be the response body -

```
{
    "registrations": [
        {
            "plate_number": "ASZX12",
            "registration": {
                "expired": false,
                "expiry_date": "2093-02-05T23:15:30.000Z"
            },
            "vehicle": {
                "type": "Wagon",
                "make": "dd",
                "model": "X4 M40i",
                "colour": "Blue",
                "vin": "12389347324",
                "tare_weight": 1700,
                "gross_mass": 123
            },
            "insurer": {
                "name": "AAMI",
                "code": 23
            }
        },
        {
            "plate_number": "SDWE23",
            "registration": {
                "expired": false,
                "expiry_date": "2093-02-05T23:15:30.000Z"
            },
            "vehicle": {
                "type": "Hatch",
                "make": "Toyota",
                "model": "Corolla",
                "colour": "Blue",
                "vin": "12389347324",
                "tare_weight": 1700,
                "gross_mass": 123
            },
            "insurer": {
                "name": "AAMI",
                "code": 23
            }
        }
    ]
}
```
Note that the database used for this project is inbuilt H2. So, no external DB installation is required.

