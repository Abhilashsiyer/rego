package com.abhi.springBootSNSW;

import com.abhi.springBootSNSW.model.Response.RegistrationRoot;
import com.abhi.springBootSNSW.model.Response.SuccessResponse;
import com.abhi.springBootSNSW.model.request.Rego;
import com.abhi.springBootSNSW.service.RegistrationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class RegistrationServiceTest {


    @Autowired
    private RegistrationService registrationService;

    ObjectMapper mapper = new ObjectMapper();

    final String testJson = "{\"plate_number\":\"ZZSER\",\"registration\":{\"expired\":false,\"expiry_date\":\"2093-02-05T23:15:30.000Z\"},"
            + "\"vehicle\":{\"type\":212,\"make\":\"BMW\",\"model\":\"X4 M40i\",\"colour\":\"Blue\",\"vin\":\"12389347324\","
            + "\"tare_weight\":1700,\"gross_mass\":123},\"insurer\":{\"name\":\"Allianz\",\"code\":23}}";

    Rego rego;

    @BeforeEach
    private void setUp() throws JsonMappingException, JsonProcessingException {

        rego = mapper.readValue(testJson, Rego.class);
    }


    @Test
    void createRegoTest() {

        SuccessResponse successResponse = registrationService.createRegistration(rego);

        assertTrue(!successResponse.getVehicleId().toString().isEmpty());


    }

    @Test
    void getRegosTest() {
        RegistrationRoot registrationRoot = registrationService.findAllRegos();

        assertTrue(registrationRoot.getRegoResponse().get(0).getPlate_number().contains("ZZSER"));

    }


}
