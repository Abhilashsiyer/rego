package com.abhi.springBootSNSW;

import com.abhi.springBootSNSW.model.Response.RegistrationRoot;
import com.abhi.springBootSNSW.model.Response.RegoResponse;
import com.abhi.springBootSNSW.model.Response.SuccessResponse;
import com.abhi.springBootSNSW.model.request.Rego;
import com.abhi.springBootSNSW.service.RegistrationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
public class RegistrationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private RegistrationService registrationService;

    @Test
    void createRegoTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        Rego rego = new Rego();
        rego.setPlate_number("DPA99F");
        String request = mapper.writeValueAsString(rego);

        Long i = (long) 1;

        SuccessResponse successResponse = new SuccessResponse(i, i, i);


        when(registrationService.createRegistration(rego)).thenReturn(successResponse);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/regos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        ).andExpect(status().isOk());

    }

    @Test
    void getAllRegosTest() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        RegistrationRoot registrationRoot = new RegistrationRoot();
        List<RegoResponse> regoResponse = new ArrayList<RegoResponse>();

        RegoResponse regoResp = new RegoResponse();

        regoResp.setPlate_number("DPA99F");

        regoResponse.add(regoResp);
        registrationRoot.getRegoResponse();
        String response = mapper.writeValueAsString(registrationRoot);

        when(registrationService.findAllRegos()).thenReturn(registrationRoot);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/regos")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(content().json(response));

    }

    @Test
    void createRegoNoRequestBodyErrorTest() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/regos")
                .contentType(MediaType.APPLICATION_JSON)
                .content("")
        ).andExpect(status().is(500))
                .andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("UNKNOWN_ERROR"));
    }

    @Test
    void createRegoWithMissingMandatoryParamsTest() throws Exception {

        final String testJson = "{\"registration\":{\"expired\":false,\"expiry_date\":\"2093-02-05T23:15:30.000Z\"},"
                + "\"vehicle\":{\"type\":212,\"make\":\"BMW\",\"model\":\"X4 M40i\",\"colour\":\"Blue\",\"vin\":\"12389347324\","
                + "\"tare_weight\":1700,\"gross_mass\":123},\"insurer\":{\"name\":\"Allianz\",\"code\":23}}";


        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/regos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testJson)
        ).andExpect(status().is(400))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("METHOD_ARGUMENT_NOT_VALID"));
    }
}
