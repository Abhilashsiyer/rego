package com.abhi.springBootSNSW;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceNswTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceNswTestApplication.class, args);
    }

}
