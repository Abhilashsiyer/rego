package com.abhi.springBootSNSW.model.Response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleResponse {
    private String type;
    private String make;
    private String model;
    private String colour;
    private String vin;
    private int tare_weight;
    private int gross_mass;
}
