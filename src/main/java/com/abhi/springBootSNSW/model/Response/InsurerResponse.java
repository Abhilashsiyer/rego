package com.abhi.springBootSNSW.model.Response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsurerResponse {
    private String name;
    private int code;
}
