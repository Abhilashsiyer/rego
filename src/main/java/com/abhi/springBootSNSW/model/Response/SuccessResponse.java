package com.abhi.springBootSNSW.model.Response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponse {
    public SuccessResponse(Long vehicleId, Long insurerId, Long registrationId) {
        super();
        this.vehicleId = vehicleId;
        this.insurerId = insurerId;
        this.registrationId = registrationId;
    }

    @JsonProperty("vehicle_ID")
    private Long vehicleId;

    @JsonProperty("insurer_ID")
    private Long insurerId;

    @JsonProperty("rego_ID")
    private Long registrationId;
}
