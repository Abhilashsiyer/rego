package com.abhi.springBootSNSW.model.Response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class RegistrationRoot {
    @JsonProperty("registrations")
    private List<RegoResponse> regoResponse;
}
