package com.abhi.springBootSNSW.model.Response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationResponse {
    private Boolean expired;
    private String expiry_date;
}
