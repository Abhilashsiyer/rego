package com.abhi.springBootSNSW.model.Response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegoResponse {

    private String plate_number;

    @JsonProperty("registration")
    private RegistrationResponse registrationResponse;

    @JsonProperty("vehicle")
    private VehicleResponse vehicleResponse;

    @JsonProperty("insurer")
    private InsurerResponse insurerResponse;


}
