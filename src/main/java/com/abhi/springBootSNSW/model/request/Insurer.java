package com.abhi.springBootSNSW.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Insurer {
    private String name;
    private int code;
}
