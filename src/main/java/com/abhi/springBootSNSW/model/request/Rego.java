package com.abhi.springBootSNSW.model.request;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class Rego {

    @NotEmpty
    private String plate_number;

    private Registration registration;

    private Vehicle vehicle;

    private Insurer insurer;


}
