package com.abhi.springBootSNSW.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Registration {
    private Boolean expired;
    private String expiry_date;
}
