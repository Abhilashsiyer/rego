package com.abhi.springBootSNSW.model.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vehicle {
    private String type;
    private String make;
    private String model;
    private String colour;
    private String vin;
    private int tare_weight;
    private int gross_mass;
}
