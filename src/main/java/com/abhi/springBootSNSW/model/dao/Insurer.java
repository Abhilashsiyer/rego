package com.abhi.springBootSNSW.model.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Insurer {

    public Insurer(String plateNumber, String name, int code) {
        super();
        this.plateNumber = plateNumber;
        this.name = name;
        this.code = code;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String plateNumber;
    private String name;
    private int code;

    public Insurer() {
        super();
    }


}
