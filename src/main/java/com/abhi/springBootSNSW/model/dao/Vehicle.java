package com.abhi.springBootSNSW.model.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Vehicle {

    public Vehicle() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(unique = true)
    private String plate_number;
    private String type;
    private String make;
    private String model;
    private String colour;
    private String vin;
    private int tare_weight;
    private int gross_mass;

    public Vehicle(String plate_number, String type, String make, String model, String colour, String vin, int tare_weight,
                   int gross_mass) {

        this.plate_number = plate_number;
        this.type = type;
        this.make = make;
        this.model = model;
        this.colour = colour;
        this.vin = vin;
        this.tare_weight = tare_weight;
        this.gross_mass = gross_mass;
    }


}
