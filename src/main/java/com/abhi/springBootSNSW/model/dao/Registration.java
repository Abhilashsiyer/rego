package com.abhi.springBootSNSW.model.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Registration {

    public Registration(String plateNumber, Boolean expired, String expiry_date) {
        super();
        this.plateNumber = plateNumber;
        this.expired = expired;
        this.expiry_date = expiry_date;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String plateNumber;
    private Boolean expired;
    private String expiry_date;

    public Registration() {
        super();
    }


}
