package com.abhi.springBootSNSW.common;

public class Constants {

    private Constants() {
    }
    public static final String DB_SAVE_SUCCESS = "DB_SAVE_SUCCESS";
    public static final String ALL_REGISTRATIONS = "ALL_REGISTRATIONS";

    public static final String DATA_INTEGRITY_VIOLATION = "DATA_INTEGRITY_VIOLATION";
    public static final String METHOD_ARGUMENT_NOT_VALID = "METHOD_ARGUMENT_NOT_VALID";
    public static final String NO_SUCH_ELEMENT = "NO_SUCH_ELEMENT";
    public static final String UNKNOWN_ERROR = "UNKNOWN_ERROR";
}
