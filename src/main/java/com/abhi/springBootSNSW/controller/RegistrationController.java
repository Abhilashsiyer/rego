package com.abhi.springBootSNSW.controller;

import com.abhi.springBootSNSW.model.Response.RegistrationRoot;
import com.abhi.springBootSNSW.model.Response.SuccessResponse;
import com.abhi.springBootSNSW.model.request.Rego;
import com.abhi.springBootSNSW.service.RegistrationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.abhi.springBootSNSW.common.Constants.ALL_REGISTRATIONS;
import static com.abhi.springBootSNSW.common.Constants.DB_SAVE_SUCCESS;

@Slf4j
@RestController
@Validated
@RequestMapping("/api/v1/regos")
public class RegistrationController {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RegistrationService registrationService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegistrationRoot> getAllRegistrations() throws JsonProcessingException {
        RegistrationRoot registrationRoot = registrationService.findAllRegos();

        log.info(ALL_REGISTRATIONS + objectMapper.writeValueAsString(registrationRoot));

        return new ResponseEntity<>(registrationRoot, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuccessResponse> createRegistrations(@Valid @RequestBody Rego registration) throws JsonProcessingException {
        SuccessResponse successResponse = registrationService.createRegistration(registration);

        log.info(DB_SAVE_SUCCESS + objectMapper.writeValueAsString(successResponse));
        return new ResponseEntity<>(successResponse, HttpStatus.OK);

    }

}
