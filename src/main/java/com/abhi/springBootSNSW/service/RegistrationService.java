package com.abhi.springBootSNSW.service;

import com.abhi.springBootSNSW.model.Response.*;
import com.abhi.springBootSNSW.model.dao.Insurer;
import com.abhi.springBootSNSW.model.dao.Registration;
import com.abhi.springBootSNSW.model.dao.Vehicle;
import com.abhi.springBootSNSW.model.request.Rego;
import com.abhi.springBootSNSW.repo.InsurerRepository;
import com.abhi.springBootSNSW.repo.RegistrationRepository;
import com.abhi.springBootSNSW.repo.VehicleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RegistrationService {
    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private InsurerRepository insuranceRepository;

    public RegistrationService() {
    }

    public SuccessResponse createRegistration(Rego rego) {

        Vehicle vehicle = new Vehicle(rego.getPlate_number(), rego.getVehicle().getType(), rego.getVehicle().getMake(),
                rego.getVehicle().getModel(), rego.getVehicle().getColour(), rego.getVehicle().getVin(),
                rego.getVehicle().getTare_weight(), rego.getVehicle().getGross_mass());

        vehicleRepository.save(vehicle);

        Registration registration = new Registration(rego.getPlate_number(), rego.getRegistration().getExpired(),
                rego.getRegistration().getExpiry_date());

        registrationRepository.save(registration);

        Insurer insurer = new Insurer(rego.getPlate_number(), rego.getInsurer().getName(), rego.getInsurer().getCode());

        insuranceRepository.save(insurer);

        SuccessResponse successResponse = new SuccessResponse(vehicle.getId(), registration.getId(), insurer.getId());

        return successResponse;
    }

    public RegistrationRoot findAllRegos() {
        RegistrationRoot registrationRoot = new RegistrationRoot();
        List<RegoResponse> regoResponse = new ArrayList<RegoResponse>();

        for (Vehicle vehicle : vehicleRepository.findAll()) {

            RegoResponse regoResp = new RegoResponse();

            VehicleResponse vehicleResp = new VehicleResponse();

            InsurerResponse insurerResponse = new InsurerResponse();

            RegistrationResponse registrationResponse = new RegistrationResponse();

            regoResp.setPlate_number(vehicle.getPlate_number());

            vehicleResp.setType(vehicle.getType());
            vehicleResp.setMake(vehicle.getMake());
            vehicleResp.setModel(vehicle.getModel());
            vehicleResp.setColour(vehicle.getColour());
            vehicleResp.setVin(vehicle.getVin());
            vehicleResp.setTare_weight(vehicle.getTare_weight());
            vehicleResp.setGross_mass(vehicle.getGross_mass());

            regoResp.setVehicleResponse(vehicleResp);

			/*
			SELECT * FROM Insurer WHERE Plate_Number = 'VEHICLE.GET_PLATE_NUMBER'
			 */
            List<Insurer> insurers = insuranceRepository.findByPlateNumber(vehicle.getPlate_number());

            insurerResponse.setName(insurers.get(0).getName());
            insurerResponse.setCode(insurers.get(0).getCode());

            regoResp.setInsurerResponse(insurerResponse);


			/*
			SELECT * FROM Registration WHERE Plate_Number = 'vehicle.getPlateNumber()'
			 */
            List<Registration> registrations = registrationRepository.findByPlateNumber(vehicle.getPlate_number());
            registrationResponse.setExpiry_date(registrations.get(0).getExpiry_date());
            registrationResponse.setExpired(registrations.get(0).getExpired());
            regoResp.setRegistrationResponse(registrationResponse);


            regoResponse.add(regoResp);

        }

        registrationRoot.setRegoResponse(regoResponse);

        return registrationRoot;
    }

}
