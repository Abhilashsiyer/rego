package com.abhi.springBootSNSW.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;

import static com.abhi.springBootSNSW.common.Constants.*;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private ResponseEntity<RegoErrorResponse> error(final HttpStatus httpStatus,
                                                    String error, Exception e) {
        return new ResponseEntity<>(new RegoErrorResponse(error, e.getMessage()), httpStatus);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RegoErrorResponse> handleRuntimeException(HttpServletRequest request, final Exception e) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR, UNKNOWN_ERROR, e);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<RegoErrorResponse> handleDataInegrityException(HttpServletRequest request, final DataIntegrityViolationException e) {
        return error(HttpStatus.BAD_REQUEST, DATA_INTEGRITY_VIOLATION, e);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<RegoErrorResponse> handleInvalidException(HttpServletRequest request, final MethodArgumentNotValidException e) {
        return error(HttpStatus.BAD_REQUEST, METHOD_ARGUMENT_NOT_VALID, e);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<RegoErrorResponse> handleInvalidException(HttpServletRequest request, final NoSuchElementException e) {
        return error(HttpStatus.NOT_FOUND, NO_SUCH_ELEMENT, e);
    }
}
