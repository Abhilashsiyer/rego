package com.abhi.springBootSNSW.repo;

import com.abhi.springBootSNSW.model.dao.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long>, QueryByExampleExecutor<Vehicle> {
}
