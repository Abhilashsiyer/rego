package com.abhi.springBootSNSW.repo;


import com.abhi.springBootSNSW.model.dao.Registration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegistrationRepository extends CrudRepository<Registration, Long> {
    List<Registration> findByPlateNumber(String plateNumber);

}
