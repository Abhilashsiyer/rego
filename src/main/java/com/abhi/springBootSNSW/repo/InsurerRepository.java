package com.abhi.springBootSNSW.repo;

import com.abhi.springBootSNSW.model.dao.Insurer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InsurerRepository extends CrudRepository<Insurer, Long> {
    List<Insurer> findByPlateNumber(String plateNumber);

}
